/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

function getInformationBoardsListsCardsAllData(board ,list , cards, boardsInformation, listInformation, cardsInformation) {
    boardsInformation(board , "mcu453ed").then((boardThanos) => {
     console.log(boardThanos)
     return listInformation(list , 'mcu453ed');
    }).then((thanosList) =>{
         console.log(thanosList);
         let allCards = [];
         thanosList.map((element )=> {
            allCards.push(cardsInformation(cards ,element.id))
         })
         return Promise.all(allCards);
        }).then((mindCardsAndCords) => {
                    console.log(mindCardsAndCords);
                }).catch(err =>{
                    console.error(err);
        })
 };
 module.exports = getInformationBoardsListsCardsAllData;