/* 
	Problem 4: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/


function getInformationBoardsListsCards(board ,list , cards, boardsInformation, listInformation, cardsInformation) {
   boardsInformation(board , "mcu453ed").then((boardThanos) => {
    console.log(boardThanos);
    return listInformation(list ,"mcu453ed");
   }).then((thanosList) =>{
        console.log(thanosList);
        return cardsInformation(cards , 'qwsa221');
    }).then((mindCards) => {
        console.log(mindCards);
    }).catch(err =>{
        console.error(err);
    })
};
module.exports = getInformationBoardsListsCards;