
/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/


function getInformationBoardsListsCardsInMindAndSpace(board ,list , cards, boardsInformation, listInformation, cardsInformation) {
    boardsInformation(board , "mcu453ed").then((boardThanos) => {
     console.log(boardThanos);
     return listInformation(list , 'mcu453ed');
    }).then((thanosList) =>{
         console.log(thanosList);
         let allCards =[];
         thanosList.map((element )=> {
            if (element.name === "Mind" || element.name === "Space") {
                 allCards.push(cardsInformation(cards , element.id));
            }
            })
            return Promise.all(allCards)
            }).then((mindCardsAndCords) => {
                    console.log(mindCardsAndCords);
                }).catch(err =>{
                    console.error(err);
                })
 };
 module.exports = getInformationBoardsListsCardsInMindAndSpace;