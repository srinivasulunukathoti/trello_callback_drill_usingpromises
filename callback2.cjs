
/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/
function listInformation(list , boardID) {
    return myPromise = new Promise((resolve , reject) =>{
        setTimeout(()=>{
            let getListData={};
            for (const data in list) {
                if (data == boardID) {
                    getListData =list[data];
                }
            }
            if (getListData) {
                console.log("successfully resolved");
                resolve(getListData);
            } else {
                console.log("unsuccessfull");
                reject(getListData);
            }
        },3*1000)
    })
};
module.exports = listInformation;